import * as ActionTypes from './actionTypes';

import { parseFile } from 'lib/parse';

export function setParsedFile(parsedFile) {
    return {
        type: ActionTypes.SET_PARSED_FILE,
        parsedFile
    };
}

export function setGrade(grade) {
    return {
        type: ActionTypes.SET_GRADE,
        grade
    };
}

export function setFormState(formState) {
    return {
        type: ActionTypes.SET_FORM_STATE,
        formState
    };
}

export function loadSourceFile(sourceFile) {
    return function(dispatch) {
        return parseFile(sourceFile)
            .then((result) => {
                dispatch(setParsedFile(result))
                dispatch(setFormState(ActionTypes.FormState.SET_CLASSES));
            });
    }
}