export const Gender = {
    MALE: 'MALE',
    FEMALE: 'FEMALE'
};

export const BranchOfEducation = {
    MU: 'MU',
    GY: 'GY'
};

export const Confession = {
    RK: 'RK',
    EV: 'EV',
    ETH: 'ETH'
};
