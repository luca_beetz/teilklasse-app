import {
    Gender,
    BranchOfEducation,
    Confession
} from './constants';

export class StudentInfo {

    /**
     * Creates a new StudentInfo object containing information about a student
     *
     * @param {Gender} gender The student's gender
     * @param {BranchOfEducation} branchOfEducation The student's branch of education
     * @param {Confession} confession The student's confession
     * @param {boolean} inFootballClass Whether the student is in a football class
     * @param {boolean} inFullDayClass Whether the student is in a whole-day class
     * @param {[string]} friends A list of the student's friends
     */
    constructor(gender, branchOfEducation, confession, inFootballClass, inFullDayClass, friends) {
        this.gender = gender;
        this.branchOfEducation = branchOfEducation;
        this.confession = confession;
        this.inFootballClass = inFootballClass;
        this.inFullDayClass = inFullDayClass;
        this.friends = friends;
    }
}