import { BranchOfEducation, Confession } from './constants';

export class SchoolClass {

    /**
     * Creates a new SchoolClass object containing information about a school class
     *
     * @param {[BranchOfEducation]} branchesOfEducation The branches of education students in
     * this class can have
     * @param {[Confession]} confessions The confessions students in this class can have
     * @param {boolean} isFootballClass Whether this is a football class
     * @param {boolean} isWholeDayClass Whether this is a whole day class
     */
    constructor(branchesOfEducation, confessions, isFootballClass, isWholeDayClass) {
        this.branchesOfEducation = branchesOfEducation;
        this.confessions = confessions;
        this.isFootballClass = isFootballClass;
        this.isWholeDayClass = isWholeDayClass;

        /**
         * List containing the names of students in this class
         */
        this.students = [];
    }
}