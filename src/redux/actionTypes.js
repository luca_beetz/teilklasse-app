export const SET_PARSED_FILE = 'SET_PARSED_FILE';
export const SET_GRADE = 'SET_GRADE';

export const SET_FORM_STATE = 'SET_FORM_STATE';

export const FormState = {
    SET_SOURCE: 'SET_SOURCE',
    SET_CLASSES: 'SET_CLASSES',
    SHOW_RESULTS: 'SHOW_RESULTS'
};