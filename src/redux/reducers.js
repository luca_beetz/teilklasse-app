import {
    SET_GRADE,
    SET_FORM_STATE,
    SET_SOURCE_FILE,
    FormState,
    SET_PARSED_FILE
} from './actionTypes';

import { combineReducers } from 'redux';

const classApp = combineReducers({
    parsedFile,
    grade,
    formState
});

/**
 * Reducer for the sourceFile property of the state
 *
 * It contains a representation of the excel/calc file
 * @param {object} state
 * @param {object} action
 */
function parsedFile(state = {}, action) {
    switch (action.type) {
        case SET_PARSED_FILE:
            return action.parsedFile;

        default:
            return state;
    }
}

/**
 * Reducer for the grade property of the state
 *
 * It contains the selected grade
 * @param {number} state
 * @param {object} action
 */
function grade(state = 0, action) {
    switch (action.type) {
        case SET_GRADE:
            return action.grade;

        default:
            return state;
    }
}

/**
 * Reducer for the formState property of the state
 *
 * It contains the state of the form, important for UI
 * @param {FormStates} state
 * @param {object} action
 */
function formState(state = FormState.SET_SOURCE, action) {
    switch (action.type) {
        case SET_FORM_STATE:
            return action.formState;

        default:
            return state;
    }
}

export default classApp;