import React, { Component } from 'react';
import { connect } from 'react-redux';

import { FormState } from 'redux/actionTypes';
import FileForm from 'components/content/forms/FileForm';

// Temporary import
import ClassForm from 'components/content/forms/classes/ClassForm';

class MainBody extends Component {
    render() {
        let form;
        console.log(this.props);

        // Render the form according to which state the app is in
        switch (this.props.formState) {
            case FormState.SET_SOURCE:
                form = <FileForm />;
                break;

            case FormState.SET_CLASSES:
                form = <ClassForm />;
                break;

            default:
                form = <FileForm />;
        }

        return (
            <div className="md:flex flex-wrap border justify-center rounded shadow-lg p-4">
                { form }
            </div>
        )
    };
};

const mapStateToProps = (state) => {
    return {
        formState: state.formState
    };
};

export default connect(
    mapStateToProps
)(MainBody);