import React from 'react';

const FormItemRadio = ({title, classCode}) => {
    const itemCode = title.concat(classCode);

    return (
        <div className="md:w-1/3 mb-6 px-3">
            <label className="flex text-gray-500 font-bold rounded border-2 border-gray-200" for={itemCode}>
                <span className="md:w-2/3 text-sm px-3 py-2">
                    { title }
                </span>
                <input className="md:w-1/3 ml-2 leading-tight" type="checkbox" id={itemCode} />
            </label>
        </div>
    );
};

export default FormItemRadio;