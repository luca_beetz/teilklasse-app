import React from 'react';

const FormItemSelect = ({title, options, classCode}) => {
    const optionItems = options.map(option =>
        <option>{ option }</option>
    );

    const itemCode = title.concat(classCode);

    return (
        <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for={itemCode}>
                { title }
            </label>
            <div className="relative">
                <select className="input-element" id={itemCode}>
                    { optionItems }
                </select>
            </div>
        </div>
    );
};

export default FormItemSelect;