import React from 'react';

import ClassFormItem from './ClassFormItem';

const ClassForm = () => {
    return (
        <form className="w-full max-w-3xl">
            <ClassFormItem classCode="a" />
            <ClassFormItem classCode="b" />
            <ClassFormItem classCode="c" />
            <ClassFormItem classCode="d" />
            <div className="md:flex md:items-center pr-3 mb-6">
                <button className="md:w-2/3 bg-teal-400 hover:bg-teal-300 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="button">
                    Weiter
                </button>
            </div>
        </form>
    );
};

export default ClassForm;