import React from 'react';

import FormItemSelect from 'components/content/forms/classes/FormItemSelect';
import FormItemRadio from 'components/content/forms/classes/FormItemCheckbox';

const ClassFormItem = (props) => {
    return (
        <div>
            <div className="mb-1 pt-2 border-t">
                <label className="text-teal-500 text-2xl font-thin">Klasse 5{props.classCode}</label>
            </div>
            <div className="flex flex-wrap -mx-3 mb-4">
                <FormItemSelect title="Konfession" classCode={props.classCode} options={['Alle', 'RK', 'EV', 'ETH']} />
                <FormItemSelect title="Bildungsgang" classCode={props.classCode} options={['Alle', 'GY', 'MU']} />
            </div>
            <div className="flex flex-wrap -mx-3">
                <FormItemRadio title="Ganztagsklasse" classCode={props.classCode} />
                <FormItemRadio title="Fußballklasse" classCode={props.classCode} />
            </div>
        </div>
    );
};

export default ClassFormItem;
