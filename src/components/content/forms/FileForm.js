import React, { Component } from 'react';
import { connect } from 'react-redux';

import { setGrade, loadSourceFile } from 'redux/actionCreators';

/**
 * Creates a form containing inputs to enter the grade (5-10) and choose the excel file
 */
class FileForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            grade: 5
        };

        this.fileInput = React.createRef();

        this.handleGradeChange = this.handleGradeChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleGradeChange(event) {
        const value = event.target.value;

        this.setState({
            grade: parseInt(value)
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        if (this.fileInput.current.files[0]) {
            this.props.setGrade(this.state.grade);
            this.props.loadSourceFile(this.fileInput.current.files[0]);
        } else {
            alert('Please select a file');
        }
    }

    render() {
        return (
            <form className="w-full max-w-3xl" onSubmit={this.handleSubmit}>
                <div className="md:flex md:items-center mb-6">
                    <div className="md:w-1/3">
                        <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
                            Jahrgangsstufe
                        </label>
                    </div>
                    <div className="md:w-2/3">
                        <select className="input-element" id="inline-class-size"
                           value={this.state.grade} onChange={this.handleGradeChange} name="grade">
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>10</option>
                        </select>
                    </div>
                </div>
                <div className="md:flex md:items-center mb-6">
                    <div className="md:w-1/3">
                        <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
                            Eingangsdatei
                        </label>
                    </div>
                    <div className="md:w-2/3">
                        <input className="input-element" id="inline-class-size" type="file"
                        ref={this.fileInput} />
                    </div>
                </div>
                <div className="md:flex md:items-center mb-6">
                    <button className="md:w-2/3 ml-auto bg-teal-400 hover:bg-teal-300 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                        Weiter
                    </button>
                </div>
            </form>
        );
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setGrade: grade => dispatch(setGrade(grade)),
        loadSourceFile: file => dispatch(loadSourceFile(file))
    }
}

export default connect(
    null,
    mapDispatchToProps
)(FileForm);