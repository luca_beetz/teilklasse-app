import React from 'react';

const NavItem = (props) => {
    return (
        <div className="flex items-center hover:bg-teal-600 cursor-pointer rounded">
            <span className="large material-icons text-white pl-3">{props.icon}</span>
            <p className="inline-block text-xl p-4 pl-2 text-white">
                {props.linkName}
            </p>
        </div>
    );
};

export default NavItem;