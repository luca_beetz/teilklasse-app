import React from 'react';

import NavItem from 'components/nav/NavItem';

const Navbar = () => {
    return (
        <nav className="flex flex-row items-center justify-between shadow-lg bg-teal-500 rounded">
            <div className="flex-grow p-2 pl-4">
                <span className="font-semibold text-3xl text-white tracking-wide">Klasseneinteilung</span>
            </div>
            <NavItem linkName="Home" icon="home" />
            <NavItem linkName="Tutorial" icon="feedback" />
            <NavItem linkName="About" icon="info" />
        </nav>
    );
};

export default Navbar;
