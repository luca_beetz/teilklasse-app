import React from 'react';

import Navbar from './components/nav/Navbar';
import MainBody from 'components/content/MainBody';

function App() {
    return (
        <div className="App antialiased container mx-auto">
            <div className="my-4">
                <Navbar />
            </div>
            <MainBody />
        </div>
    );
}

export default App;
