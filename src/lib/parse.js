import readXlsxFile from 'read-excel-file';
import { StudentInfo } from 'redux/model/StudentInfo';
import { Gender, BranchOfEducation, Confession } from 'redux/model/constants';

const schema = {
    'Name': {
        prop: 'name',
        type: String
    },
    'Gesch.': {
        prop: 'gender',
        type: String
    },
    'Bildungsg.': {
        prop: 'branch',
        type: String
    },
    'Rel.': {
        prop: 'confession',
        type: String
    },
    'Fußballkl.': {
        prop: 'inFootballClass',
        type: String
    },
    'Ganztag': {
        prop: 'inFullDayClass',
        type: String
    },
    'Freunde': {
        prop: 'friends',
        type: {
            'Freund1': {
                prop: '0',
                type: String
            },
            'Freund2': {
                prop: '1',
                type: String
            },
            'Freund3': {
                prop: '2',
                type: String
            }
        }
    }
}

export const parseFile = async (file) => {
    const { rows, errors } = await readXlsxFile(file, { schema });

    const students = {};
    for (const row of rows) {
        students[row['name']] = parseRow(row);
    }
    return students;
};

const parseRow = (row) => {
    console.log('Parse row called');

    const studentInfo = new StudentInfo(
        row['gender'].toUpperCase(),
        row['branch'].toUpperCase(),
        row['confession'].toUpperCase(),
        row['inFootballClass'] === 'J' ? true : false,
        row['inFullDayClass'] === 'J' ? true : false
    );

    console.log(studentInfo);

    return studentInfo;
}

const keyExists = (value, keys) => {
    for (const key of keys) {
        if (value.toUpperCase() === key) {
            return true;
        }
    }

    return false;
}